﻿using Android.Media;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherMetronome.Metronome
{
    public class AudioGenerator
    {
        private int _sampleRate;
        private AudioTrack audioTrack;

        public AudioGenerator(int sampleRate)
        {
            _sampleRate = sampleRate;
        }
        public double[] getSineWave(int samples, int sampleRate, double frequencyOfTone)
        {
            double[] sample = new double[samples];
            for (int i = 0; i < samples; i++)
            {
                sample[i] = Math.Sin(2 * Math.PI * i / (sampleRate / frequencyOfTone));
            }
            return sample;
        }

        public byte[] get16BitPcm(double[] samples)
        {
            byte[] generatedSound = new byte[2 * samples.Length];
            int index = 0;
            foreach (double sample in samples)
            {
                // scale to maximum amplitude
                
                short maxSample = (short)((sample * Int16.MaxValue));
                // in 16 bit wav PCM, first byte is the low order byte
                generatedSound[index++] = (byte)(maxSample & 0x00ff);
                generatedSound[index++] = (byte)((maxSample & 0xff00) >> 8);

            }
            return generatedSound;
        }

        public void createPlayer()
        {   
            audioTrack = new AudioTrack(Stream.Music, _sampleRate, ChannelOut.Mono, Android.Media.Encoding.Pcm16bit, _sampleRate, AudioTrackMode.Stream);

            audioTrack.Play();
        }

        public Task<int> writeSound(double[] samples)
        {
            byte[] generatedSnd = get16BitPcm(samples);
            return audioTrack.WriteAsync(generatedSnd, 0, generatedSnd.Length);
            
        }

        public void destroyAudioTrack()
        {
            audioTrack.Stop();
            audioTrack.Release();
        }

    }
}
