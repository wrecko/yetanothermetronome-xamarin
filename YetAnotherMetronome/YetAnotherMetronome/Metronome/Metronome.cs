﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherMetronome.Metronome
{
    public class Metronome
    {
        private double _bpm = 100;
        private int _beat;
        private int _noteValue;
        private int _silence;

        private double _beatSound;
        private double _sound;
        private int _tick = 1000; // samples of tick

        public bool Play;

        private AudioGenerator audioGenerator = new AudioGenerator(8000);

        public Metronome()
        {
            audioGenerator.createPlayer();
        }

        public double SetTempo(double tempo) => _bpm = tempo;

        public void calcSilence()
        {
            _silence = (int)(((60 / _bpm) * 8000) - _tick);
        }

        public async void play()
        {
            calcSilence();
            double[] tick = audioGenerator.getSineWave(this._tick, 8000, _beatSound);
            double[] tock = audioGenerator.getSineWave(this._tick, 8000, _sound);
            double silence = 0;
            double[] sound = new double[8000];
            int t = 0, s = 0, b = 0;            
            do
            {
                for (int i = 0; i < sound.Length; i++)
                {
                    if (t < this._tick)
                    {
                        if (b == 0)
                            sound[i] = tock[t];
                        else
                            sound[i] = tick[t];
                        t++;
                    }
                    else
                    {
                        sound[i] = silence;
                        s++;
                        if (s >= this._silence)
                        {
                            t = 0;
                            s = 0;
                            b++;
                            if (b > (this._beat - 1))
                                b = 0;
                        }
                    }
                }
                await audioGenerator.writeSound(sound);
            } while (Play);
        }

        public void stop()
        {
            Play = false;
            audioGenerator.destroyAudioTrack();
        }
    }
}
