﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YetAnotherMetronome.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ExercisePage : ContentPage
	{
        private Metronome.Metronome _metronome;

		public ExercisePage ()
		{
			InitializeComponent();
            Title = "Exercise";

            _metronome = new Metronome.Metronome();
            tempo.Value = 100;

            ToolbarItems.Add(new ToolbarItem("Filter", "filter.png", async () => 
            {
                var page = new ContentPage(); var result = await page.DisplayAlert("Title", "Message", "Accept", "Cancel");
            }));

        }

        private void tempo_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            seeTempo.Text = $"Value : {e.NewValue}";
            _metronome.SetTempo(e.NewValue);
        }

        private void startBeat_Clicked(object sender, EventArgs e)
        {
            _metronome.Play = !_metronome.Play;

            if(_metronome.Play)
                _metronome.play();
        }
    }
}